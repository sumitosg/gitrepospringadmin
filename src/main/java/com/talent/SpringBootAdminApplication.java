package com.talent;
import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAdminServer
public class SpringBootAdminApplication {
    private static final Logger logger = LoggerFactory.getLogger(SpringBootAdminApplication.class);
    public static void main(String[] args) {
        logger.debug("::::::::::::::: SERVER RUNNING ::::::::::::::::");
        SpringApplication.run(SpringBootAdminApplication.class, args);
    }
}